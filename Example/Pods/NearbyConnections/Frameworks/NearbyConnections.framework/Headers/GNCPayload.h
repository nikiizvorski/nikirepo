#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// This class encapsulates a Bytes payload.
@interface GNCBytesPayload : NSObject

/// The unique identifier of the payload.
@property(nonatomic, readonly) int64_t identifier;

/// The content of the payload.
@property(nonatomic, readonly) NSData *bytes;

/// Creates a Bytes payload object.
/// Note: To maximize performance, @c bytes is strongly referenced, not copied.
+ (instancetype)payloadWithBytes:(NSData *)bytes;

@end


/// This class encapsulates a Stream payload.
@interface GNCStreamPayload : NSObject

/// The unique identifier of the payload.
@property(nonatomic, readonly) int64_t identifier;

/// The payload data is read from this input stream.
@property(nonatomic, readonly) NSInputStream *stream;

+ (instancetype)payloadWithStream:(NSInputStream *)stream;

@end


/// This class encapsulates a File payload.
@interface GNCFilePayload : NSObject

/// The unique identifier of the payload.
@property(nonatomic, readonly) int64_t identifier;

/// A URL that identifies the file.
@property(nonatomic, readonly, copy) NSURL *fileURL;

+ (instancetype)payloadWithFileURL:(NSURL *)fileURL;

@end

NS_ASSUME_NONNULL_END
