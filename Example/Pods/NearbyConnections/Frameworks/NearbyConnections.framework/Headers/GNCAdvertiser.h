#import <Foundation/Foundation.h>

#import "GNCConnection.h"

NS_ASSUME_NONNULL_BEGIN

/// This contains info about a discoverer endpoint intitiating a connection with an advertiser.
@protocol GNCAdvertiserConnectionInfo <NSObject>
/// This is a human readable name of the discoverer.
@property(nonatomic, readonly, copy) NSString *name;
/// This token can be used to verify the identity of the discoverer.
@property(nonatomic, readonly, copy) NSString *authToken;
@end

/// This handler is called when a discoverer requests a connection with an advertiser.  In
/// response, the advertiser should accept or reject via @c responseHandler.
/// @param endpointId The ID of the endpoint.
/// @param connectionInfo Information about the discoverer.
/// @param responseHandler Handler for the connection response, which is either an acceptance or
///                        rejection of the connection request.
typedef void (^GNCAdvertiserConnectionInitiationHandler)(
    GNCEndpointId endpointId,
    id<GNCAdvertiserConnectionInfo> connectionInfo,
    GNCConnectionResponseHandler responseHandler);


/// An advertiser broadcasts a service that can be seen by discoverers, which can then make
/// requests to connect to it. Release the advertiser object to stop advertising.
@interface GNCAdvertiser : NSObject

/// Factory method that creates an advertiser.
/// @param name A human readable name of this endpoint, to be displayed on other endpoints.
/// @param serviceId A string that uniquely identifies the advertised service.
/// @param strategy The connection topology to use.
/// @param connectionInitiationHandler A handler that is called when a discoverer requests a
///                                    connection with this endpoint.
+ (instancetype)advertiserWithName:(NSString *)name
                         serviceId:(NSString *)serviceId
                          strategy:(GNCStrategy)strategy
       connectionInitiationHandler:(GNCAdvertiserConnectionInitiationHandler)
           connectionInitiationHandler;

@end

NS_ASSUME_NONNULL_END
