#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class GNCBytesPayload, GNCStreamPayload, GNCFilePayload;

/// Result of a connection request.
typedef NS_ENUM(NSInteger, GNCConnectionResult) {
  GNCConnectionResultRejected,   ///< the endpoint rejected the connection request
  GNCConnectionResultAccepted,   ///< the endpoint accepted the connection request
};

/// Handler for a @c GNCConnectingResult value.
typedef void (^GNCConnectionResultHandler)(GNCConnectionResult);


/// Reasons that a connection can be severed by either endpoint.
typedef NS_ENUM(NSInteger, GNCDisconnectedReason) {
  GNCDisconnectedReasonUnknown,  ///< the endpoint can no longer be reached
};

/// Handler for a @c GNCDisconnectedReason value.
typedef void (^GNCDisconnectedHandler)(GNCDisconnectedReason);


/// Result of a payload transfer.
typedef NS_ENUM(NSInteger, GNCPayloadResult) {
  GNCPayloadResultSuccess,    ///< Payload delivery was successful.
  GNCPayloadResultFailure,    ///< An error occurred during payload delivery.
  GNCPayloadResultCanceled,   ///< Payload delivery was canceled.
};

/// Handler for a @c GNCPayloadResult value.
typedef void (^GNCPayloadResultHandler)(GNCPayloadResult);


/// Connection topology.  See https://developers.google.com/nearby/connections/strategies.
typedef NS_ENUM(NSInteger, GNCStrategy) {
  GNCStrategyCluster,       ///< M-to-N
  GNCStrategyStar,          ///< 1-to-N
  GNCStrategyPointToPoint,  ///< 1-to-1
};


/// Every endpoint has a unique identifier.
typedef NSString *GNCEndpointId;


/// This handler receives a Bytes payload, signifying the start of receipt of the data.  The
/// progress object can be used to monitor progress or cancel the operation.  This handler must
/// return a completion handler, which is called when the operation is finished.  When finished,
/// the received data can be accessed in the payload object.
typedef GNCPayloadResultHandler _Nonnull (^GNCBytesPayloadHandler)(GNCBytesPayload *payload,
                                                                   NSProgress *progress);

/// This handler receives a Stream payload, signifying the start of receipt of a stream. The payload
/// data should be read from the supplied input stream. The progress object can be used to monitor
/// progress or cancel the operation.  This handler must return a completion handler, which is
/// called when the operation is finished.
typedef GNCPayloadResultHandler _Nonnull (^GNCStreamPayloadHandler)(GNCStreamPayload *payload,
                                                                    NSProgress *progress);

/// This handler receives a File payload, signifying the start of receipt of a file.  The
/// progress object can be used to monitor progress or cancel the operation.  This handler must
/// return a completion handler, which is called when the operation finishes successfully or if
/// there is an error.  The file will be stored in a temporary location. If an error occurs or the
/// operation is canceled, the file will contain all data that was received. It is the client's
/// responsibility to delete the file when it is no longer needed.
typedef GNCPayloadResultHandler _Nonnull (^GNCFilePayloadHandler)(GNCFilePayload *payload,
                                                                  NSProgress *progress);


/// This class contains optional handlers for a connection.
@interface GNCConnectionHandlers : NSObject

/// This handler receives Bytes payloads.  It is optional; apps that don't send and receive Bytes
/// payloads need not supply this handler.
@property(nonatomic, nullable) GNCBytesPayloadHandler bytesPayloadHandler;

/// This handler receives a stream that delivers a payload in chunks.  It is optional; apps that
/// don't send and receive Stream payloads need not supply this handler.
@property(nonatomic, nullable) GNCStreamPayloadHandler streamPayloadHandler;

/// This handler receives a File payload.  It is optional; apps that don't send and receive File
/// payloads need not supply this handler.
/// Note: File payloads are not yet supported.
@property(nonatomic, nullable) GNCFilePayloadHandler filePayloadHandler;

/// This handler is called when the connection is ended, whether due to the endpoint disconnecting
/// or moving out of range.  It is optional.
@property(nonatomic, nullable) GNCDisconnectedHandler disconnectedHandler;

/// This factory method lets you specify a subset of the connection handlers in a single expression.
/// @param builderBlock Set up the handlers in this block.
+ (instancetype)handlersWithBuilder:(void (^)(GNCConnectionHandlers *))builderBlock;

@end


/// This represents a connection with an endpoint.  Use it to send payloads to the endpoint, and
/// release it to disconnect.
@protocol GNCConnection <NSObject>

/// Send a Bytes payload.  A progress object is returned, which can be used to monitor
/// progress or cancel the operation.  |completion| will be called when the operation completes
/// (in all cases, even if failed or was canceled).
- (NSProgress *)sendBytesPayload:(GNCBytesPayload *)payload
                      completion:(GNCPayloadResultHandler)completion;

/// Send a Stream payload.  A progress object is returned, which can be used to monitor
/// progress or cancel the operation.  The stream data is read from the supplied NSInputStream.
/// |completion| will be called when the operation completes.
- (NSProgress *)sendStreamPayload:(GNCStreamPayload *)payload
                       completion:(GNCPayloadResultHandler)completion;

/// Send a File payload.  A progress object is returned, which can be used to monitor progress or
/// cancel the operation.  |completion| will be called when the operation completes.
/// Note: File payloads are not yet supported.
- (NSProgress *)sendFilePayload:(GNCFilePayload *)payload
                     completion:(GNCPayloadResultHandler)completion;
@end

/// This handler takes a @c GNCConnection object and returns a @c GNCConnectionHandlers
/// object containing the desired payload and connection-ended handlers.
typedef GNCConnectionHandlers * _Nonnull(^GNCConnectionHandler)(id<GNCConnection> connection);


/// This class contains a response (acceptance or rejection) to an initiated connection.
@interface GNCConnectionResponse : NSObject

/// This factory method creates an acceptance of a connection request.
/// @param acceptHandler This handler is called if the other endpoint also accepts the connection.
///                      A @c GNCConnection object is passed, meaning that the connection has
///                      been established and you may start sending and receiving payloads.
/// @param rejectHandler This handler is called if the other endpoint rejects the connection.
+ (instancetype)acceptanceWithAcceptHandler:(GNCConnectionHandler)acceptHandler
                              rejectHandler:(GNCConnectionResultHandler)rejectHandler;

/// This factory method creates a rejection of a connection request.
/// @param resultHandler This handler is called with the other endpoint's response.
+ (instancetype)rejectionWithResultHandler:(GNCConnectionResultHandler)resultHandler;

/// This factory method creates a rejection of a connection request.
+ (instancetype)rejection;

@end

/// This handler takes a @c GNCConnectionResponse object.
typedef void (^GNCConnectionResponseHandler)(GNCConnectionResponse *response);

NS_ASSUME_NONNULL_END
