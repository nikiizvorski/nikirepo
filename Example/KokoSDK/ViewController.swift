//
//  ViewController.swift
//  KokoSDK
//
//  Created by nikiizvorski@gmail.com on 07/30/2018.
//  Copyright (c) 2018 nikiizvorski@gmail.com. All rights reserved.
//

import UIKit
import KokoSDK

class ViewController: UIViewController {
    let data = MyClass()
    
    @IBAction func onClick(_ sender: Any) {
        data.bad()
    }
    
    var isBlinking = false
    let replaceMe = ReplaceMe(frame: CGRect(x: 10, y: 20, width: 200, height: 30))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        // Setup the BlinkingLabel
        replaceMe.text = "I blink!"
        replaceMe.font = UIFont.systemFont(ofSize: 20)
        view.addSubview(replaceMe)
        replaceMe.startBlinking()
        isBlinking = true
        
        // Create a UIButton to toggle the blinking
        let toggleButton = UIButton(frame: CGRect(x: 10, y: 60, width: 125, height: 30))
        toggleButton.setTitle(replaceMe.text, for: .normal)
        toggleButton.setTitleColor(UIColor.red, for: .normal)
        toggleButton.addTarget(self, action: Selector(("toggleBlinking")), for: .touchUpInside)
        view.addSubview(toggleButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func toggleBlinking() {
        if (isBlinking) {
            replaceMe.stopBlinking()
        } else {
            replaceMe.startBlinking()
        }
        isBlinking = !isBlinking
    }

}

