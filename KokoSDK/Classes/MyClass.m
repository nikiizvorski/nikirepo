//
//  MyClass.m
//  KokoSDK
//
//  Created by Niki Izvorski on 30/07/2018.
//

#import "MyClass.h"
#import <CommonCrypto/CommonCrypto.h>
#import "NearbyConnections/NearbyConnections.h"

static NSString *kServiceId = @"co.buckleup.safety";

@interface MyClass ()
@property(nonatomic) GNCAdvertiser *advertiser;
@property(nonatomic) GNCDiscoverer *discoverer;
@end

@implementation MyClass

- (void) bad {
    NSString *nov = @"{\"generatorId\":\"8d2be4b7d9b8096916fea4db1b59eff9\",\"id\":3781,\"text\":\"help me\",\"timestamp\":1533259974000,\"type\":1}";
    
    NSLog(@"Nearby Connection Advertiser Init");
    
        _advertiser = [GNCAdvertiser advertiserWithName:nov
                                serviceId:kServiceId
                                 strategy:GNCStrategyCluster
              connectionInitiationHandler:^(GNCEndpointId endpointId,
                                            id<GNCAdvertiserConnectionInfo> advConnInfo,
                                            GNCConnectionResponseHandler responseHandler) {
                  // Make the nav bar blue to show that a discoverer has connected.

                  NSLog(@"Nearby Connection started advertising");
              }];
}

-(void)startDiscovering: (onEndpointFoundCallback) callback; {
    
    NSLog(@"Nearby Connection Discovery Init");
    
    _discoverer =[GNCDiscoverer discovererWithServiceId:kServiceId
                                  strategy:GNCStrategyCluster
                      endpointFoundHandler:^(GNCEndpointId endpointId,
                                             id<GNCDiscoveredEndpointInfo> endpointInfo) {
                          __weak typeof(self) weakSelf = self;
                          
                          NSLog(@"Nearby Connection started discovering");
                          
                          // An endpoint was discovered; add it to the endpoint list and UI.
                          //                          self.endpoints[endpointId] = [[EndpointInfo alloc] initWithDiscoveredInfo:endpointInfo];
                          //                          [self.tableView reloadData];
                          
                          
                          // Return the lost handler for this endpoint.
                          
                          
                          if(endpointId != nil && endpointInfo.name != nil){
                              NSLog(@"Nearby Connection inside callback");
                              callback(endpointId, endpointInfo.name);
                          }
                          return ^{
                              typeof(self) self = weakSelf;  // shadow
                              // Endpoint disappeared; remove it from the endpoint list and UI.
                              //                              [self.endpoints removeObjectForKey:endpointId];
                              //                              [self.tableView reloadData];
                          };
                      }];
}

+ (NSString *)myMethod {
    NSString *key = @"test";
    NSString *data = @"mytestdata";
    const char *ckey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cdata = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char chmac[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, ckey, strlen(ckey), cdata, strlen(cdata), chmac);
    NSData *hmac = [[NSData alloc] initWithBytes:chmac length:sizeof(chmac)];
    NSString *hash = [hmac base64EncodedStringWithOptions:0];
    return hash;
}

+ (NSString *)novMethod {
    NSString *key = @"test";
    NSString *data = @"mytestdata";
    const char *ckey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cdata = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char chmac[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, ckey, strlen(ckey), cdata, strlen(cdata), chmac);
    NSData *hmac = [[NSData alloc] initWithBytes:chmac length:sizeof(chmac)];
    NSString *hash = [hmac base64EncodedStringWithOptions:0];
    return hash;
}

@end
