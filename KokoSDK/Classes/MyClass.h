//
//  MyClass.h
//  Pods
//
//  Created by Niki Izvorski on 30/07/2018.
//

#import <Foundation/Foundation.h>


@interface MyClass : NSObject

typedef void (^onEndpointFoundCallback)(NSString *endpointId, NSString *endpointName);
-(void)startDiscovering: (onEndpointFoundCallback) callback;
    
- (void) bad;

+ (NSString *)myMethod;

+ (NSString *)novMethod;

@end
