#
# Be sure to run `pod lib lint KokoSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KokoSDK'
  s.version          = '0.6.7'
  s.summary          = 'A short description of KokoSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/nikiizvorski/nikirepo'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'nikiizvorski@gmail.com' => 'dev@airdates.co' }
  s.source           = { :git => 'https://nikiizvorski@bitbucket.org/nikiizvorski/nikirepo.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'KokoSDK/Classes/*.{h,m,swift}'
  
  s.frameworks = 'CoreBluetooth','LocalAuthentication', 'NearbyConnections'
  s.vendored_frameworks = 'NearbyConnections.framework'
  s.static_framework = true

  s.ios.deployment_target = '8.0'
  
  s.library = 'sqlite3','c++'
  
  # Specify the frameworks we are providing.
  # The app using this Pod should _not_ link these Frameworks,
  # they are bundled as a part of this Pod for technical reasons.
#  s.vendored_frameworks = 'NearbyConnections.framework'

  # LDFLAGS required by Firebase dependencies.
  s.pod_target_xcconfig = {
      'FRAMEWORK_SEARCH_PATHS' => '/Applications/Xcode.app/Contents/Developer/Library/Frameworks',
      'OTHER_LDFLAGS' => '$(inherited) -ObjC'
  }
  
  # s.resource_bundles = {
  #   'KokoSDK' => ['KokoSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
#  s.dependency 'NearbyConnections', '~> 0.0.9'

s.dependency 'GoogleToolboxForMac/Logger', '~> 2.1'
#  s.dependency 'NearbyConnections', '~> 0.0.9'
end
