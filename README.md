# KokoSDK

[![CI Status](https://img.shields.io/travis/nikiizvorski@gmail.com/KokoSDK.svg?style=flat)](https://travis-ci.org/nikiizvorski@gmail.com/KokoSDK)
[![Version](https://img.shields.io/cocoapods/v/KokoSDK.svg?style=flat)](https://cocoapods.org/pods/KokoSDK)
[![License](https://img.shields.io/cocoapods/l/KokoSDK.svg?style=flat)](https://cocoapods.org/pods/KokoSDK)
[![Platform](https://img.shields.io/cocoapods/p/KokoSDK.svg?style=flat)](https://cocoapods.org/pods/KokoSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KokoSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KokoSDK'
```

## Author

nikiizvorski@gmail.com, dev@airdates.co

## License

KokoSDK is available under the MIT license. See the LICENSE file for more info.
