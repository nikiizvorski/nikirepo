#import <Foundation/Foundation.h>

#import "GNCConnection.h"

NS_ASSUME_NONNULL_BEGIN

/// This is info about an advertiser endpoint with which the discoverer has requested a connection.
@protocol GNCDiscovererConnectionInfo <NSObject>
/// This token can be used to verify the identity of the advertiser.
@property(nonatomic, readonly, copy) NSString *authToken;
@end

/// This handler is called to establish authorization with the advertiser.  In response,
/// @c responseHandler should be called to accept or reject the connection.
/// @param connectionInfo Information about the advertiser.
/// @param responseHandler Handler for the connection response, which is either an acceptance or
///                        rejection of the connection request.
typedef void (^GNCDiscovererConnectionInitializationHandler)(
    id<GNCDiscovererConnectionInfo> connectionInfo,
    GNCConnectionResponseHandler responseHandler);

/// This handler should be called to request a connection with an advertiser.
/// @param name A human readable name of this endpoint, to be displayed on the other endpoint.
/// @param authorizationHandler This handler is called establish authorization.
typedef void (^GNCConnectionRequester)(
    NSString *name,
    GNCDiscovererConnectionInitializationHandler connectionAuthorizationHandler);

/// This contains info about a discovered advertiser endpoint.
@protocol GNCDiscoveredEndpointInfo <NSObject>
/// This is a human readable name of the advertiser.
@property(nonatomic, readonly, copy) NSString *name;
/// Call this block to request a connection with the advertiser.
@property(nonatomic, readonly) GNCConnectionRequester requestConnection;
@end

/// This handler is called when a previously discovered advertiser endpoint is lost.
typedef void (^GNCEndpointLostHandler)();

/// This handler is called when an advertiser endpoint is discovered.
/// @param endpointId The ID of the endpoint.
/// @param connectionInfo Information about the endpoint.
/// @return Block that is called when the endpoint is lost.
typedef GNCEndpointLostHandler _Nonnull (^GNCEndpointFoundHandler)(
    GNCEndpointId endpointId,
    id<GNCDiscoveredEndpointInfo> endpointInfo);

/// A discoverer searches for endpoints advertising the specified service, and allows connection
/// requests to be sent to them. Release the discoverer object to stop discovering.
@interface GNCDiscoverer : NSObject

/// Factory method that creates a discoverer.
/// @param serviceId A string that uniquely identifies the advertised service to search for.
/// @param strategy The connection topology to use.
/// @param endpointFoundHandler This handler is called when an endpoint advertising the service is
///                             discovered.
+ (instancetype)discovererWithServiceId:(NSString *)serviceId
                               strategy:(GNCStrategy)strategy
                   endpointFoundHandler:(GNCEndpointFoundHandler)endpointFoundHandler;

@end

NS_ASSUME_NONNULL_END
